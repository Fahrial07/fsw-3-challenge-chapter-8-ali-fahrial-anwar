# Heroku Link

https://binar-car-ch8-alifarial.herokuapp.com

# Screenshots Testing
![test-result](/test-result.png)



# BCR API

Di dalam repository ini terdapat implementasi API dari Binar Car Rental.
Tugas kalian disini adalah:
1. Fork repository
2. Tulis unit test di dalam repository ini menggunakan `jest`.
3. Coverage minimal 70%

Good luck!
